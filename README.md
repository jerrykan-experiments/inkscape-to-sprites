# Prototype: Inkscape Sprite Sheet Tool
A simple prototype tool for creating
[SVG "sprite sheets"](https://jerrykan-experiments.gitlab.io/web-svg-icons/)
from an [Inkscape](https://inkscape.org/) document.

## Example

    $ python3 inkscape-to-sprites.py svg/svg_icon_set.svg > sprites.svg

## Assumptions
The tool makes a few assumptions about the input Inkscape file:
  - each icon path is located on its own layer
  - each layer contains only one path.
    - if multiple paths exist in a layer, only the first will be used.
  - each icon is located in a grid of "boxes" 100units x 100units.
  - each icon is centred within its own 100 x 100 "box" and correctly scaled to fit.
  - the icon name is derived from the layer name and contains only letters,
    numbers, spaces, or hyphens.
    - layer names will be converted to lowercase and spaces replaced with
      hyphens.
  - the neither of the x/y coordinates of an icon path's first "move" direction
    is exactly divisible by 100.
    - there is an edge case where if one of x/y coordinates of the "move"
      direction is exactly divisible by 100, it's `viewBox` values could be
      incorrectly set.
    - the current work-around is the resize the icon path to 99.999% of its
      original size and recenter in its grid "box".

### Future Work
Each of these assumptions has been made simply to reduce the complexity of the
script that is used to generate the "sprite sheets". Future work could be
undertaken to remove these assumptions, but that work is not currently being
considered.
