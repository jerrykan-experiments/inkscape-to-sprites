#!/usr/bin/env python3

import math
from xml.dom.minidom import parse
from xml.etree.ElementTree import Element, SubElement, tostring


def extract_icon_paths(svg_dom):
    paths = {}

    for group in svg_dom.getElementsByTagName('g'):
        if not group.getAttribute('inkscape:groupmode') == 'layer':
            continue

        name = group.getAttribute('inkscape:label')
        if not name:
            continue

        try:
            path = group.getElementsByTagName('path')[0]
        except IndexError:
            continue

        shape = path.getAttribute('d')
        if not shape:
            continue

        name = name.lower().replace(' ', '-')
        paths[name] = shape

    return paths


def construct_svg_tree(paths, previews=False):
    grid_size = math.ceil(math.sqrt(len(paths)))
    root = Element('svg')

    # root <svg> element
    root.set('xmlns', 'http://www.w3.org/2000/svg')
    if previews:
        root.set('xmlns:xlink', 'http://www.w3.org/1999/xlink')

    # <symbol> elements
    for name, shape in paths.items():
        cmd, coords, _ = shape.split(' ', maxsplit=2)
        x, y = [((int(float(i)) // 100) * 100) for i in coords.split(',')]

        symbol = SubElement(root, 'symbol')
        symbol.set('id', name)
        symbol.set('viewBox', '{} {} 100 100'.format(x, y))

        path = SubElement(symbol, 'path')
        path.set('d', shape)

    # "preview" <use> elements
    if previews:
        for i, name in enumerate(paths):
            preview = SubElement(root, 'use')
            preview.set('xlink:href', '#{}'.format(name))
            preview.set('x', str(i % grid_size * 100))
            preview.set('y', str(i // grid_size * 100))
            preview.set('width', '100')
            preview.set('height', '100')

    return root


def _print_help():
    print('Usage:')
    print('    {} <inkscape file>'.format(sys.argv[0]))
    print()
    print('note: SVG sprite file will be output to STDOUT')


if __name__ == '__main__':
    import sys

    if len(sys.argv) < 2:
        _print_help()
        sys.exit(1)

    try:
        with open(sys.argv[1]) as inkscape_file:
            inkscape_tree = parse(inkscape_file)
    except OSError as e:
        print("{} '{}'".format(e.strerror, e.filename))
        sys.exit(2)

    paths = extract_icon_paths(inkscape_tree)
    sprite_svg = construct_svg_tree(paths, True)

    print(tostring(sprite_svg).decode())
